from datetime import datetime, timedelta
from summary import Summary
from duration import Duration
from helpers import get_data_duration

DATE_FORMAT = '%m/%d/%y, %I:%M:%S %p'

# TODO make sure this scenarios are covered by unit tests
# end_time > start_time (happy path)
# end_time < start_time 
# end_time = start_time 
def get_data_duration(raw_data):
    start_time = datetime.strptime(raw_data.get('Start Time'), DATE_FORMAT) # or datetime.strptime(raw_data.get('First Join Time'), DATE_FORMAT)
    end_time = datetime.strptime(raw_data.get('End Time'), DATE_FORMAT)# or datetime.strptime(raw_data.get('Last Leave time'), DATE_FORMAT)
    diff = end_time - start_time

    # divmod (cociente/residuo de la division)
    hours, remainder = divmod(diff.seconds, 3600)
    minutes, seconds = divmod(remainder, 60)
    duration = {'hours':hours, 'minutes':minutes, 'seconds':seconds}
    return duration

def normalize_summary(raw_data: dict):
    duration = get_data_duration(raw_data)

    # TODO: Line 22-35 is same as Line 36-48, don't repeat yourself

    # TODO for literals that I am using / referencing more than once 'Meeting title'
    if not raw_data.get('Meeting title') or raw_data.get('Meeting title') == 'General':
        result = {
            'Title' : 'General',
            'Id' : raw_data.get('Id'),
            # TODO take care of default values when None wherever is required
            'Attended participants' : int(raw_data.get('Attended participants') or 0),
            'Start Time' : raw_data.get('Start Time'),
            'End Time' : raw_data.get('End Time'),
            'Duration': {
                'hours': duration.get('hours'),
                'minutes': duration.get('minutes'),
                'seconds': duration.get('seconds'),
            }
        }
        return result
    result = {
        'Title' : raw_data.get('Meeting title'),
        'Id' : raw_data.get('Meeting title'),
        'Attended participants' : int(raw_data.get('Attended participants') or 0),
        'Start Time' : raw_data.get('Start Time'),
        'End Time' : raw_data.get('End Time'),
        'Duration' : {
            'hours' : duration.get('hours'),
            'minutes' : duration.get('minutes'),
            'seconds' : duration.get('seconds'),
        }
    }
    return result














def build_summary_object(raw_data: dict):
    pass